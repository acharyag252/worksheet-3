import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class geometry3DTest {

	@Test
	void testCuboidVolume() {
		geometry3D cal=new geometry3D();
		int a=2;
		int b=3; 
		int c=4;

		double actual=cal. cuboidVolume (a, b, c) ;

		double expected=24;

		assertEquals (expected , actual);
	}

	@Test
	void testCuboidArea() {
		geometry3D cal=new geometry3D();

		int a=2;
		int b=3; 
		int c=4;

		double actual=cal. cuboidArea (a, b, c) ;

		double expected=52;

		assertEquals (expected , actual);
	}

	@Test
	void testPyramidVolume() {
		geometry3D cal=new geometry3D(); 
		int a=2;
		int b=3;

		double actual=cal.pyramidVolume (a, b) ;

		double expected=4;

		assertEquals(expected, actual);
	}

	@Test
	void testPyramidArea() {
		geometry3D cal=new geometry3D(); 
		int a=2;
		int b=3;

		double actual=cal.pyramidArea (a, b) ;

		double expected=16.65;

		assertEquals(expected, actual, 0.01) ;
	}

	@Test
	void testTetraVolume() {
		geometry3D cal=new geometry3D();

		int a=2;

		double actual=cal.tetraVolume (a);

		double expected=0.94;

		assertEquals (expected, actual, 0.01);
	}

	@Test
	void testTetraArea() {
		geometry3D cal=new geometry3D();

		int a=2;

		double actual=cal.tetraAre(a);

		double expected=6.93;

		assertEquals (expected, actual, 0.01);
	}

}
