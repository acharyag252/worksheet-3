
public class geometry3D {
	public double cuboidVolume (double l, double w, double h){

		return (1*w*h);
	}

		public double cuboidArea(double l, double w, double h){

		return (2* (l*w+1*h+h*w)) ;
		}
	

		public double pyramidVolume (double a, double h){

		return (((double)1/3)*(a*a)*h);
		}

		public double pyramidArea (double a, double h){

		return (a*a)+a*Math.sqrt (a*a+4*h*h);
		}

		public double tetraVolume(double a){

		return ((a*a*a)/(6*Math.sqrt (2)));
		}

		public double tetraAre(double a){

		return (Math.sqrt (3)* (a*a));
}
}